/*populate database*/

/*TABLES FOR THE API*/
INSERT INTO `footballapi`.`match_status` (`id`, `status_name`) VALUES ('1', 'SCHEDULED'), ('2', 'FINISHED'); 

/*TABLES FOR THE CRON*/
INSERT INTO `footballapi`.`task_status` (`id`, `name`) VALUES ('1', 'STARTED'), ('2', 'FINISHED'); 

INSERT INTO `footballapi`.`type_cron` (`id`, `name`) VALUES ('1', 'GET_SCHEDULED'), ('2', 'GET_FINISHED'); 