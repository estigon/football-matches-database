/*CREATE DATABASE*/
CREATE DATABASE `footballapi`;

/*TABLES FOR THE API*/
CREATE TABLE `footballapi`.`match_status` ( `id` INT NOT NULL AUTO_INCREMENT , `status_name` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `footballapi`.`team` ( `id` INT NOT NULL, `team_name` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `footballapi`.`match` 
( 
`id` INT NOT NULL, 
`date` DATE,
`winner` VARCHAR(50),
`score` VARCHAR(50),
`match_status_id` INT(11) NOT NULL,
`home_team` INT(11) NOT NULL,
`away_team` INT(11) NOT NULL,
foreign key (`match_status_id`) references match_status(id) on delete cascade on update cascade, 
foreign key (`home_team`) references team(id) on delete cascade on update cascade, 
foreign key (`away_team`) references team(id) on delete cascade on update cascade, 
PRIMARY KEY (`id`)) ENGINE = InnoDB; 

/*TABLES FOR THE CRON*/

CREATE TABLE `footballapi`.`task_status` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `footballapi`.`type_cron` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `footballapi`.`task` 
( 
`id` INT NOT NULL AUTO_INCREMENT, 
`error` VARCHAR(300) NULL DEFAULT NULL, 
`type_id` INT(11) NOT NULL, 
`task_status_id` INT(11) NOT NULL,
foreign key (`type_id`) references type_cron(id) on delete cascade on update cascade,
foreign key (`task_status_id`) references task_status(id) on delete cascade on update cascade, 
PRIMARY KEY (`id`)) ENGINE = InnoDB; 
