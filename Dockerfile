FROM mysql:5.7.38
LABEL authors="elysestiben@gmail.com"
ENV MYSQL_ROOT_PASSWORD 1234
ADD data/ /docker-entrypoint-initdb.d

EXPOSE 3306