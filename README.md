# Match Football DATABASE

## run in the project folder to create image
```
docker build -t custom_mysql .
```

## create container
```
docker run --detach --name=football_mysql --publish 6603:3306 custom_mysql
```

## open container in the command prompt
```
docker exec -it football_mysql bash
```

## Settings for "football-matches-cron" and "football-matches-api"

## inside both proyects edit the .env file as in the example below

```
DATABASE=footballapi
DATABASE_PORT=6603
DATABASE_HOST=localhost
USERNAMEDB=root
PASSWORD=1234
```

### NOTES
If you choose a different port when creating the container, you must add it in the .env file instead of the one shown in the example